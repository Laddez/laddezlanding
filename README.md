# Tateeda website#

### How to start? ###

Project builded with nodejs + gulp

1. Install all dependencies using the command `npm install`
2. Run project `npm start`. This command run gulp task **watch** to track files changes.


Only for the build use the command `npm run build`

### Folder ###

- **assets** - all designs, mocks, psd
- **build** - builded project. Minified css, js.
- **public** - general project folder