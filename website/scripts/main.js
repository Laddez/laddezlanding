$(function() {
    $(document).on("scroll", function(){
        $("input:focus").blur();
    });
    var $body = $('body');  
    var $navbar = $('.navbar');  
    var navbarHeight = 80;

    function topScroll() {
        if ($(window).scrollTop() <= 60) {
            $body.addClass('top-scroll');
        } else {
            $body.removeClass('top-scroll');
        }
    }

    function scrollNavbar(){
        var $logoColor = $('#logo-color');
        var $logoWhite = $('#logo-white');

        if ($(document).width() >= 768 && $(window).scrollTop() >= navbarHeight) {
            $navbar.addClass('scroll-navbar');
            $logoColor.removeClass('logo-hidden');
            $logoWhite.addClass('logo-hidden');
        } else {
            $navbar.removeClass('scroll-navbar');
            $logoWhite.removeClass('logo-hidden');
            $logoColor.addClass('logo-hidden');
        }
    }

    // topScroll();
    scrollNavbar();

    $(window).scroll(function() {
        scrollNavbar();
    })



    $('[data-page-scroll]').bind('click', function(event) {
        var target = $(this).attr('data-page-scroll') || $(this).attr('href');
        var $target = $(target)
        var $navbar = $('.navbar-fixed-top');

        if (!$target.length) {
            window.location.href = '/' + target;
        }

        var scrollTo = $target.offset().top - ($navbar.length ? $navbar.height() : 0);

        if ($target.length) {
            $('html, body').stop().animate({
                scrollTop: (scrollTo)
            }, 1250, 'easeInOutExpo');
        }
        event.preventDefault();
        return false;
    });

    var $carouselVideo = $('#carousel-video');

    $carouselVideo.find('video').bind('ended', function() {
        $carouselVideo.carousel('next');
        playCarouselVideo();
    })

    $carouselVideo.bind('slid.bs.carousel', function() {
        var $nextItem = $carouselVideo.find('.item.active');
        $('.js-hero-caption').html($nextItem.find('.carousel-caption').html());
    })

    function playCarouselVideo() {
        var $nextItem = $carouselVideo.find('.item.active');
        var video = $nextItem.find('video')[0];
        video.currentTime = 0;
        video.play();
    }

    if (!isMobile()) {
        playCarouselVideo();
        $carouselVideo.find('video').show();
        $carouselVideo.find('.img-poster').hide();
        if (!isSafari() && !isFirefox() && !isMicrosoft()){
        new MaterialLayers(".section", { wrap: true });
        }
    } else {
        $carouselVideo.carousel({
            interval: 10000
          })
    }
    $.fn.preload("/images/posters/pay-for-the-results.webm_snapshot.jpg","/images/posters/get-more-real-users.webm_snapshot.jpg",
     "/images/posters/be-with-experienced-specialists.webm_snapshot.jpg","/images/posters/be-with-the-best-team.webm_snapshot.jpg");
});

function isMobile() {
    try{ document.createEvent("TouchEvent"); return true; }
    catch(e){ return false; }
}
function isSafari(){
    return (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0);
}
function isFirefox(){
    return (navigator.userAgent.search("Firefox") > -1);
}
function isMicrosoft(){
    return (navigator.userAgent.search("Edge") > -1 || navigator.userAgent.search("Microsoft Internet Explorer") > -1);
}