$(function() {
    $('.logos-carousel').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        focusOnSelect: true,
        mobileFirst: true,
        dots: false,
        swipeToSlide: true,
        prevArrow: '<button class="logo-arrow logo-arrow__prev"><span class="glyphicon glyphicon-menu-left"></span></button>',
        nextArrow: '<button class="logo-arrow logo-arrow__next"><span class="glyphicon glyphicon-menu-right"></span></button>',
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true
            },

        }, {
            breakpoint: 1200,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
                arrows: true
            }
        }]
    });
})