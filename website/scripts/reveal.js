$(function() {
        // Initialize and Configure Scroll Reveal Animation
    window.sr = ScrollReveal();

    // sr.reveal('.about', {
    //     duration: 600,
    //     scale: 0.6,
    //     distance: '0px'
    // }, 400);

    sr.reveal('.service', {
        duration: 600,
        scale: 0.6,
        distance: '0px',
        viewFactor: 0.7
    }, 400);

    sr.reveal('.btn-hero', {
        duration: 600,
        scale: 0.6,
        distance: '0px'
    }, 400);

    sr.reveal('.steps-list .step', {        
        duration: 600,
        scale: 0.6,
        distance: '0px',
        viewFactor: 0.7
    }, 500)

    sr.reveal('.animation-number', {
        beforeReveal: function(elm) {
            var value = parseInt(elm.innerHTML.replace(/[.\,]/g, ''));
            var step = value / 800;
            
            $(elm).animate({ num: value }, {
                duration: 800,
                step: function (num, event){
                    this.innerHTML = (num + step).toFixed(0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                },
                complete: function() {
                    this.innerHTML = (value).toFixed(0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                }
            });
        }
    })
})