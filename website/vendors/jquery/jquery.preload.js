﻿$.fn.preload = function preloadImages() {
    var allImages = [];
    var allImagesLoad = [];

    for (var i = 0; i < preloadImages.arguments.length; i++) {
        allImagesLoad.push(
            new Promise(function (resolve, reject) {
                allImages[i] = new Image()
                allImages[i].onload = function () {
                    resolve();
                }
                allImages[i].src = preloadImages.arguments[i];
            })
        );
    }

    return Promise.all(allImagesLoad);
}