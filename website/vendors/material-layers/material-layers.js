function MaterialLayers(selector, options) {
    this.options = {
        wrap: options.wrap || false,
        offset: options.offset || 1
    };
    this.items = [];
    this.selector = selector;
    this.init();
}

MaterialLayers.prototype = {
    scrollHandler: function (e) {
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        var scrollVisible = scrollTop + window.innerHeight;
        var options = this.options;

        [].forEach.call(this.items, function (item, i) {
            var visiblePersent =
                (scrollTop + window.innerHeight - item.offsetTop) / window.innerHeight;
            var itemHeight = item.offsetHeight >= window.innerHeight ? window.innerHeight : item.offsetHeight;
            var itemHiddenSize = (item.offsetTop + itemHeight) - scrollVisible;
            if (itemHiddenSize > 0) {
                item.style.transform = "translate3d(0, -" + itemHiddenSize + "px, 0)";
            } else {
                item.style.transform = '';
            }
        });
    },
    init: function (wrap) {
        var that = this;
        this.items = document.querySelectorAll(this.selector);
        
        if (this.options.wrap) {
            this.items = this.wrapSelectors();
        }

        if (this.items) {
            [].forEach.call(this.items, function (item, i) {
                item.style.position = 'relative';
                item.style.zIndex = - i - 1;
                item.style.boxShadow = '0 5px 10px rgba(0,0,0,0.3)';
            })
            this.scrollHandler();
            document.addEventListener("scroll", this.scrollHandler.bind(this), false);
            window.addEventListener("resize", this.scrollHandler.bind(this), false);
        }
    },
    wrapSelectors: function() {
        return [].map.call(this.items, function (item) {
            var wrapper = document.createElement('div');
            var parent = item.parentElement;
            wrapper.classList.add('material-layout-wrapper');
            parent.insertBefore(wrapper, item);
            wrapper.appendChild(item);
            return wrapper;
        })
    }
};
